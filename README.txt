Tripal Eimage
--------------
This module is an extension module of the Tripal module. It uses the 
eimage table from the Expression Module of Chado to associate images 
with various other content types such as stocks and features. It adds 
several custom linker tables to support the association. Images are stored 
on the file system.

Features
--------------
 - Create 'chado_eimage' nodes for data stored in the chado 'Eimage' table
 - Keep data in sync between chado and drupal's public schemas. Remove 
   orphaned eimage nodes if desired.

Required Modules
--------------
 - Drupal 6.x
 - Tripal Core Module

Installation
--------------
1. Copy the module to the Drupal 'modules' directory (e.g. /sites/all/modules). 
After enabling the module, go to 'Tripal Management' > 'Eimage' > 
'Configuration' and submit a job to sync chado eimages with Drupal. This 
will create Drupal nodes with the 'chado_eimage' node type. 

2. Copy the theme file (tripal_eimage/theme/node-chado_eimage.tpl.php) and 
the theme folder (tripal_eimage/theme/tripal_eimage) to your current theme 
directory. You can customize the display by editing the copied theme files.
