<?php
/**
 * @file
 * Template file for the Eimage node.
 */

drupal_add_css('./tripal-node-templates.css');
$eimage  = $variables['node']->eimage;

// Get the template settings.
$template_settings = theme_get_settings('tripal');

// Toggle the sidebar if desired.
$no_sidebar = 0;
if (is_array($template_settings['tripal_no_sidebar']) && $template_settings['tripal_no_sidebar']['eimage']):
  $no_sidebar = 1;
endif

?>

<?php
if ($teaser):
  print theme('tripal_eimage_teaser', $node);
else: ?>

<script type="text/javascript">
  Drupal.behaviors.eimageBehavior = function (context){
    <?php if($no_sidebar): ?>
      // Hide the resource side bar and strech the details section.
      $(".tripal_toc").hide();
      $(".tripal_details").addClass("tripal_details_full");
      $(".tripal_details_full").removeClass("tripal_details"); <?php
    else: ?>
      // Use default resource sidebar.
      $(".tripal-info-box").hide(); <?php
    endif ?>

    // Iterate through all of the info boxes and add their titles
    // to the table of contents.
    $(".tripal-info-box-title").each(function(){
      var parent = $(this).parent();
      var id = $(parent).attr('id');
      var title = $(this).text();
      $('#tripal_eimage_toc_list').append('<li><a href="#'+id+'" class="tripal_eimage_toc_item">'+title+'</a></li>');
    });

    // When a title in the table of contents is clicked, then
    // show the corresponding item in the details box.
    $(".tripal_eimage_toc_item").click(function(){
       $(".tripal-info-box").hide();
       href = $(this).attr('href');
       $(href).fadeIn('slow');
       // We want to make sure our table of contents and the details
       // box stay the same height.
       $("#tripal_eimage_toc").height($(href).parent().height());
       return false;
    }); 

    // We want the base details to show up when the page is first shown 
    // unless the user specified a specific block.
    var block = window.location.href.match(/[\?|\&]block=(.+?)\&/)
    if(block == null){
       block = window.location.href.match(/[\?|\&]block=(.+)/)
    }
    if(block != null){
       $("#tripal_eimage-"+block[1]+"-box").show();
    } else {
       $("#tripal_eimage-base-box").show();
    }

    $("#tripal_eimage_toc").height($("#tripal_eimage-base-box").parent().height());
  };
</script>

<div id="tripal_eimage_details" class="tripal_details">
  <!-- Basic Details Theme -->
  <?php print theme('tripal_eimage_base', $node); ?>

  <!-- Resource Blocks CCK elements --><?php
  for($i = 0; $i < count($node->field_resource_titles); $i++):
    if($node->field_resource_titles[$i]['value']): ?>
      <div id="tripal_eimage-resource_<?php print $i?>-box" class="tripal_eimage-info-box tripal-info-box">
        <div class="tripal_eimage-info-box-title tripal-info-box-title"><?php print $node->field_resource_titles[$i]['value'] ?></div>
        <?php print $node->field_resource_blocks[$i]['value']; ?>
      </div><?php
    endif;
  endfor;?>

  <!-- Let modules add more content -->

  <?php print $content ?>
</div>

<!-- Table of contents -->
<div id="tripal_eimage_toc" class="tripal_toc">
  <div id="tripal_eimage_toc_title" class="tripal_toc_title">Resources</div>
  <ul id="tripal_eimage_toc_list" class="tripal_toc_list">

    <!-- Resource Links CCK elements --><?php
    for($i = 0; $i < count($node->field_resource_links); $i++):
      if($node->field_resource_links[$i]['value']):
        $matches = preg_split("/\|/", $node->field_resource_links[$i]['value']);?>
        <li><a href="<?php print $matches[1] ?>" target="_blank"><?php print $matches[0] ?></a></li><?php
      endif;
    endfor;?>

    <!-- Add customized <li> links here. -->
  </ul>
</div>

<?php endif ?>
